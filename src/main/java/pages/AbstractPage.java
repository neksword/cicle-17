package pages;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;

public interface AbstractPage {

    static <T extends AbstractPage> T create(ChromeDriver driver, Class<T> page) {
        return PageFactory.initElements(driver, page);
    }

    void isLoaded();
}