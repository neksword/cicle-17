package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class KardyvachLakePage implements AbstractPage {

    private ChromeDriver driver;
    @FindBy(className = "detail-head")
    private WebElement kardyvachLakeHeader;

    KardyvachLakePage(ChromeDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @Override
    public void isLoaded() {
        assertTrue(kardyvachLakeHeader.isDisplayed());
    }
}
