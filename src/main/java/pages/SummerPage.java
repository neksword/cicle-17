package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.FindBy;
import pages.tracking.lite.*;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class SummerPage implements AbstractPage {

    private ChromeDriver driver;
    @FindBy(xpath = "//a[text()='Подобрать программу']")
    private WebElement pickProgramLink;
    @FindBy(id = "our-programs")
    private WebElement ourProgramsBlock;
    @FindBy(xpath = "//a[text()='Бзерпинский Карниз 1 день']")
    private WebElement bzerpinEavesTour;
    @FindBy(xpath = "//a[text()='Озеро Кардывач']")
    private WebElement kardyvachLakeTour;
    @FindBy(linkText = "ДЗЫХРИНСКИЙ ВОДОПАД")
    private WebElement liteTracikngDzychrinskyWaterfall;
    @FindBy(linkText = "ВОДОПАД КЕЙВА  - КРУГОЗОР ЕФРЕМОВА")
    private WebElement liteTrackingEfremovHorizon;
    @FindBy(linkText = "ПРОГУЛКА ПО ДОЛИНЕ Р. АЧИПСЕ")
    private WebElement liteTrackingRiverAchipseWalk;
    @FindBy(linkText = "ХРЕБЕТ АИБГА")
    private WebElement liteTrackingAibgaRidge;
    @FindBy(linkText = "ТАИНСТВЕННЫЕ ВОДОПАДЫ АЖЕК")
    private WebElement liteTrackingAzhekWaterfalls;
    @FindBy(linkText = "ВОДОПАД КЕЙВА")
    private WebElement liteTrackingKeivWaterfall;
    @FindBy(linkText = "ОЗЕРА ХМЕЛЕВСКОГО И АЧИПСИНСКИЕ ВОДОПАДЫ")
    private WebElement liteTrackingKhmelevsyLakeAndAchipsinskyWaterfalls;

    public SummerPage(ChromeDriver driver) {
        this.driver = driver;
    }

    public void clickPickProgram() {
        pickProgramLink.click();
        assertTrue(ourProgramsBlock.isDisplayed());
    }

    public BzerpinEavesPage loadBzerpinEavesTourPage() {
        bzerpinEavesTour.click();
        return new BzerpinEavesPage(driver);
    }

    public KardyvachLakePage loadKardyvachLakePage() {
        kardyvachLakeTour.click();
        return new KardyvachLakePage(driver);
    }

    public DzychrinckyWaterfallPage loadDzychrinckyWaterfallPage() {
        liteTracikngDzychrinskyWaterfall.click();
        return new DzychrinckyWaterfallPage(driver);
    }

    public EfremovHorizonPage loadEfremovHorizonPage() {
        liteTrackingEfremovHorizon.click();
        return new EfremovHorizonPage(driver);
    }

    public AibgaRidgePage loadAibgaRidgePage () {
        liteTrackingAibgaRidge.click();
        return new AibgaRidgePage(driver);
    }

    public AzhekWaterfallsPage loadAzhekWaterfallsPage () {
        liteTrackingAzhekWaterfalls.click();
        return new AzhekWaterfallsPage(driver);
    }

    public KeivWaterfallPage loadKeivWaterfallPage () {
        liteTrackingKeivWaterfall.click();
        return new KeivWaterfallPage(driver);
    }

    public KhmelevskyLakeAndAchipsinskyWaterfallsPage loadKhmelevskyLakeAndAchipsinskyWaterfallsPage () {
        liteTrackingKhmelevsyLakeAndAchipsinskyWaterfalls.click();
        return new KhmelevskyLakeAndAchipsinskyWaterfallsPage(driver);
    }

    public RiverAchipseWalkPage loadRiverAchipseWalkPage () {
        liteTrackingRiverAchipseWalk.click();
        return new RiverAchipseWalkPage(driver);
    }

    @Override
    public void isLoaded() {
        return;
    }
}
