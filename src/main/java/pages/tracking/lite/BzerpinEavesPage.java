package pages.tracking.lite;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.FindBy;
import pages.AbstractPage;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class BzerpinEavesPage implements AbstractPage {

    private ChromeDriver driver;
    @FindBy(xpath = "//div[@class='detail-head' and contains(text(),'Бзерпинский Карниз 1 день')]")
    private WebElement bzerpinEavesHeader;

    public BzerpinEavesPage(ChromeDriver driver) {
        this.driver = driver;
    }

    @Override
    public void isLoaded() {
        assertTrue(bzerpinEavesHeader.isDisplayed());
    }

}
