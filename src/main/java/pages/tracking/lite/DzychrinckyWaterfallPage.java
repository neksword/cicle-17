package pages.tracking.lite;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.FindBy;
import pages.AbstractPage;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class DzychrinckyWaterfallPage implements AbstractPage {

    private ChromeDriver driver;
    @FindBy(className = "detail-head")
    private WebElement dzychrinskyWaterfallHeader;

    public DzychrinckyWaterfallPage(ChromeDriver driver) {
        this.driver = driver;
    }

    @Override
    public void isLoaded() {
        assertTrue(dzychrinskyWaterfallHeader.isDisplayed());
    }
}
