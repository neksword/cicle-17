package pages.tracking.lite;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.FindBy;
import pages.AbstractPage;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class KeivWaterfallPage implements AbstractPage {

    private ChromeDriver driver;
    @FindBy(className = "detail-head")
    private WebElement keivWaterfallHeader;

    public KeivWaterfallPage(ChromeDriver driver) {
        this.driver = driver;
    }

    @Override
    public void isLoaded() {
        assertTrue(keivWaterfallHeader.isDisplayed());
    }
}
