import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class BaseTest {

    public static ChromeDriver driver;

    @BeforeEach
    public void prepare() {
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("http://summer.kpmountainclub.ru/");
    }

    @AfterEach
    public void close() {
        driver.close();
    }
}
