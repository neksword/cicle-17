import org.junit.jupiter.api.Test;
import pages.AbstractPage;
import pages.SummerPage;

public class LiteTrackingTest extends BaseTest {

    @Test
    public void DzychrinskyWaterfallTest() {
        SummerPage summerPage = AbstractPage.create(driver, SummerPage.class);
        summerPage.loadDzychrinckyWaterfallPage().isLoaded();
    }

    @Test
    public void AibgaRidgeTest() {
        SummerPage summerPage = AbstractPage.create(driver, SummerPage.class);
        summerPage.loadAibgaRidgePage().isLoaded();
    }

    @Test
    public void AzhekWaterfallsTest() {
        SummerPage summerPage = AbstractPage.create(driver, SummerPage.class);
        summerPage.loadAzhekWaterfallsPage().isLoaded();
    }

    @Test
    public void EfremovHorizonTest() {
        SummerPage summerPage = AbstractPage.create(driver, SummerPage.class);
        summerPage.loadEfremovHorizonPage().isLoaded();
    }

    @Test
    public void KeivWaterfallTest() {
        SummerPage summerPage = AbstractPage.create(driver, SummerPage.class);
        summerPage.loadKeivWaterfallPage().isLoaded();
    }

    @Test
    public void KhmelevskyLakeAndAchipsinskyWaterfallsTest() {
        SummerPage summerPage = AbstractPage.create(driver, SummerPage.class);
        summerPage.loadKhmelevskyLakeAndAchipsinskyWaterfallsPage().isLoaded();
    }

    @Test
    public void RiverAchipseWalkTest() {
        SummerPage summerPage = AbstractPage.create(driver, SummerPage.class);
        summerPage.loadRiverAchipseWalkPage().isLoaded();
    }
}
